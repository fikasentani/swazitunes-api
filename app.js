require('dotenv').config();
const path = require("path");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const cors = require('cors');

const userRoutes = require("./routes/user");
const userProfileRoutes = require("./routes/user-profile");
const songsRoutes = require('./routes/songs');
const tunesRoutes = require('./routes/tunes');

const dbUrl = process.env.DB_URL || "mongodb://localhost/swazitunes"

mongoose.set('useNewUrlParser', true);
  mongoose.set('useFindAndModify', false);
  mongoose.set('useCreateIndex', true);
  mongoose.set('useUnifiedTopology', true);
mongoose.connect(dbUrl, {useNewUrlParser: true})
.then(() => {
    console.log('Connected to Mongo Database succesfully')
})
.catch(() => {
    console.log("FAILED TO CONNECT!!");
});
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use("/images", express.static(path.join("uploads/images")));

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, x-Requested-With, Content-Type, Accept, Authorization");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, PUT, DELETE, OPTIONS");
    next();
});

app.get("/", function(req, res) {
    res.send("working");
});

app.use("/api/auth", userRoutes);
app.use("/api/user-profile", userProfileRoutes);
app.use("/api/songs", songsRoutes);
app.use("/api/tunes", tunesRoutes);

module.exports = app;

