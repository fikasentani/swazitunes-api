const User = require("../models/user");
exports.getProfile = (req, res, next) => {
    User.findOne({_id: req.params.id}, {firstName: 1, lastName: 1, imagePath: 1}).then(profile => {
        console.log('professionalfile',profile);
        if (profile) {
          res.status(200).json(profile);
        } else {
          res.status(404).json({ message: "File not found!" });
        }
      }).catch(error => {
        res.status(500).json({
          message: 'Technical error retriving your profile!'
        });
      });
}

exports.updateProfile = (req, res, next) => {
  let imagePathUrl = removeHttp(req.body.imagePath);
  const userProfile = new User({
    _id: req.body.id,
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    imagePath: imagePathUrl,
  });
  User.updateOne({ _id: req.params.id }, userProfile).then(result => {
    if(result.n > 0) {
      res.status(200).json({ message: "Profile Updated successfully!" });
    } else {
      res.status(401).json({ message: 'Update not Authorized!'})
    }
  }).catch(error => {
    res.status(500).json({
      message: 'Techncical error occured while updating profile!'
    });
  });
}