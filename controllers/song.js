const Song = require("../models/song");

exports.updateSong = (req, res, next) => {
    // let imagePathUrl = removeHttp(req.body.imagePath);
    let songPathUrl = removeHttp(req.body.songPath);
    const userProfile = new User({
      _id: req.body.id,
      title: req.body.title,
      artist: req.body.artist,
    //   imagePath: imagePathUrl,
      songPath: songPathUrl,
    });
    User.updateOne({ _id: req.params.id }, userProfile).then(result => {
      if(result.n > 0) {
        res.status(200).json({ message: "Profile Updated successfully!" });
      } else {
        res.status(401).json({ message: 'Update not Authorized!'})
      }
    }).catch(error => {
      res.status(500).json({
        message: 'Techncical error occured while updating profile!'
      });
    });
  }