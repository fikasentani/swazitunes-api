const mongoose = require("mongoose");
const uniqueValidator = require("mongoose-unique-validator");

const userSchema = mongoose.Schema({
    email: { type: String, required: true, unique: true},
    password: { type: String, required: true},
    firstName: { type: String},
    lastName: { type: String },
    imagePath: {type: String }
    
});


// userProfile: [
//     {
//         firstName: { type: String},
//         lastName: { type: String },
//         profileImage: {type: String }
//   }
// ]
userSchema.plugin(uniqueValidator);

module.exports = mongoose.model("User", userSchema);