const mongoose = require("mongoose");

const userProfileSchema = mongoose.Schema({
    // _id: {type: String},
    firstName: { type: String},
    lastName: { type: String },
    imagePath: {type: String},
    creator: { type: mongoose.Schema.Types.ObjectId, ref: "User", required: true }
});

module.exports = mongoose.model("UserProfile", userProfileSchema);