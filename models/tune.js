const mongoose = require('mongoose');

const tuneSchema = mongoose.Schema({
    title: { type: String, required: true },
    artist: { type: String, required: true },
    imagePath: { type: String, required: true },
    tunePath: { type: String, required: true },
    creator: { type: mongoose.Schema.Types.ObjectId, ref: 'User', required: true }
});

module.exports = mongoose.model('Tune', tuneSchema);