const express = require("express");

const Tune = require("../models/tune");
const checkAuth = require('../middleware/check-auth');

const router = express.Router();

router.post("", checkAuth, (req, res, next) => {
  const tune = new Tune({
    title: req.body.title,
    artist: req.body.artist,
    imagePath: req.body.imagePath,
    tunePath: req.body.tunePath,
    creator: req.userData.userId
  });
  tune.save().then(createdTune => {
    res.status(201).json({
      message: "File added successfully",
      tuneId: createdTune._id
    });
  });
});

router.patch("/:id", checkAuth, (req, res, next) => {
  const tune = new Tune({
    _id: req.body.id,
    title: req.body.title,
    artist: req.body.artist,
    imagePath: req.body.imagePath,
    tunePath: req.body.tunePath,
    creator: req.userData.userId
  });
  Tune.updateOne({ _id: req.params.id }, tune).then(result => {
    res.status(200).json({ message: "File Updated successfully!" });
  });
});

router.get("", (req, res, next) => {
  Tune.find().then(documents => {
    res.status(200).json({
      message: "Files fetched successfully!",
      tunes: documents
    });
  });
});

router.get("/:id", (req, res, next) => {
  Tune.findById(req.params.id).then(tune => {
    if (tune) {
      res.status(200).json(tune);
    } else {
      res.status(404).json({ message: "File not found!" });
    }
  });
});

router.delete("/:id", checkAuth, (req, res, next) => {
  Tune.deleteOne({ _id: req.params.id }).then(result => {
    console.log(result);
    res.status(200).json({ message: "File deleted!" });
  });
});

module.exports = router;
