const express = require('express');
const multer = require('multer');

const Song = require('../models/song');
const SongController = require("../controllers/song");
const checkAuth = require('../middleware/check-auth');

const router = express.Router();

router.patch("/:id/update-song", SongController.updateSong);

const MIME_TYPE_MAP = {
    "image/png": "png",
    "image/jpeg": "jpg",
    "image/jpg": "jpg"
  };
  
  const storage = multer.diskStorage({
      destination: (req, file, cb) => {
          const isValid = MIME_TYPE_MAP[file.mimetype];
          let error = new Error('Invalid mime type');
          if (isValid) {
              error = null;
          }
          cb(error, 'uploads/images');
      },
      filename: (req, file, cb) => {
          const name = file.originalname
          .toLowerCase()
          .split(' ')
          .join('-');
          const ext = MIME_TYPE_MAP[file.mimetype];
          cb(null, `${name}-${Date.now()}.${ext}`);
      }
  });

  router.post(
      '',
      checkAuth,
      multer({ storage: storage }).single('image'),
      (req, res, next) => {
          const url = `${req.protocol}://${req.get('host')}`;
          const song = new Song({
              title: req.body.title,
              artist: req.body.artist,
              imagePath: `${url}/images/${req.file.filename}`,
              creator: req.userData.userId
          });
          song.save().then(createdSong => {
              res.status(201).json({
                  message: 'Song added successfully',
                  song: {
                      ...createdSong,
                      id: createdSong._id
                  }
              });
          });
      });

      router.put(
          '/:id',
          checkAuth,
          multer({ storage: storage}).single('image'),
          (req, res, next) => {
              let imagePath = req.body.imagePath;
              if (req.file) {
                  const url = `${req.protocol}://${req.get('host')}`;
                  imagePath = `${url}/images/${req.file.filename}`;
              }
              const song = new Song({
                  _id: req.body.id,
                  title: req.body.title,
                  artist: req.body.artist,
                  imagePath: req.body.imagePath,
                  createdSong: req.userData.userId
              });
              Song.updateOne(
                  { _id: req.params.id, creator: req.userData.userData },
                  song
              ).then(result => {
                  if(result.nModified > 0 ) {
                      res.status(200).json({message: 'Update successful!'});
                  } else {
                      res.status(401).json({ message: 'Not authorized'});
                  }
              });
          }
      );

      router.get('', (req, res, next) => {
          const pageSize = +req.query.pageSize;
          const currentPage = +req.query.page;
          const songQuery = Song.find();
          let fetchedSongs;
          if (pageSize && currentPage) {
              songQuery.skip(pageSize * (currentPage - 1)).limit(pageSize);
          }
          songQuery
          .then(documents => {
              fetchedSongs = documents;
              return Song.count();
          })
          .then(count => {
              res.status(200).json({
                  message: "Songs fetched successfully!",
                  songs: fetchedSongs,
                  maxSongs: count
              });
          });
      });

      router.get(':id', (req, res, next) => {
          Song.findById(req.params.id).then(song => {
              if (song) {
                  res.status(200).json(song);
              } else {
                  res.status(404).json({ message: 'Song not found!'});
              }
          });
      });

      router.delete('/:id', checkAuth, (req, res, next) => {
          Song.deleteOne({ _id: req.params.id, creator: req.userData.userId }).then(
              result => {
                  console.log('result-->',result);
                  if (result.n > 0) {
                      res.status(200).json({ message: 'Song Deleted Successfully'});
                  } else {
                      res.status(401).json({ message: 'Not Authorized to delete!'});
                  }
              }
          );
      });

      module.exports = router;