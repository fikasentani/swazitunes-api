const express = require("express");
const multer = require("multer");
var ObjectId = require('mongodb').ObjectID;

const UserProfile = require("../models/user-profile");
const checkAuth = require("../middleware/check-auth");

const router = express.Router();

const MIME_TYPE_MAP = {
    "image/png": "png",
    "image/jpeg": "jpg",
    "image/jpg": "jpg"
  };

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const isValid = MIME_TYPE_MAP[file.mimetype];
        let error = new Error("Invalid mime type");
        if (isValid) {
            error = null;
        }
        cb(error, "uploads/images");
    },
    filename: (req, file, cb) => {
        const name = file.originalname.toLowerCase().split(' ').join('-');
        const ext = MIME_TYPE_MAP[file.mimetype];
        cb(null, name + "-" + Date.now() + "." + ext);
        // cb(null, `${name}-${Date.now()}.${ext}`);
    }
});

router.get('/user-profile', (req, res, next) => {
    UserProfile.find()
    .then(documents => {
        res.status(200).json({
            message: "User profile fectchec successfully",
            userProfile: documents
        });
    });
    
});

// router.get('/user-profile/:_id', (req, res, next) => {
//     UserProfile.find({_id: req.params._id})
//     .then(documents => {
//         res.status(200).json({
//             message: "User profile fectchec successfully",
//             userProfile: documents
//         });
//     });
    
// });

router.get('/user-profile/:_id', (req, res, next) => {
    UserProfile.findById(req.params._id).then(post => {
      if (post) {
        res.status(200).json(post);
      } else {
        res.status(404).json({ message: "User details not found!" });
      }
    });
  });

router.post('/user-profile',checkAuth,
multer({ storage: storage }).single('image'),
(req, res, next) => {
    const url = `${req.protocol}:${req.get("host")}`;
    const userProfile = new UserProfile({
        firstName: req.body.firstName,
        lastName: req.body.lastName,
        profileImage: `${url}/images/req.file.filename`,
        creator: req.userData.userId
    });
    userProfile.save()
    .then(result => {
        res.status(201).json({
            message: 'Profile updated successfully',
            result: result
        });
    })
    .catch(err => {
        res.status(500).json({
            error: err
        });
    })
});

router.put("/user-profile/:id", checkAuth, multer({storage: storage}).single('image'), (req, res, next) => {
    let imagePath = req.body.imagePath;
    if (req.file) {
      const url = req.protocol + "://" + req.get("host");
      console.log('url-->', url);
      
      imagePath = url + "/images/" + req.file.filename
      console.log('imgpath-->', imagePath);
      
    } else {
        console.log('no file bro');
        
    }
    const userInfo = new UserProfile ({
        _id: req.body.id,
            firstName: req.body.firstName ,
        lastName: req.body.lastName,
        imagePath: imagePath,
        creator: req.userData.userId
    })
    UserProfile.updateOne({ _id: req.params.id, creator: req.userData.userId }, userInfo).then(result => {
       res.status(200).json({message: 'Updated Successfully!'})
   }).catch(err => {
       console.log(err + ' didnt work');
   }) 

   // UserProfile.findByIdAndUpdate(req.params._id, userInfo, function(err, updatedInfo) {
    //     if(err) {
    //         res.status(500).json({messahe: 'not successful'})
    //     } else {
    //         res.status(200).json({message: 'Updated Successfully!'});
    //     }
    // })
})

module.exports = router;